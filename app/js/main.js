$(document).ready(function() {
    $('.title-item').on('click', function() {
        $('.title-item').removeClass('selected');
        $(this).addClass('selected');

        $.ajax({
            url: 'titles_manager.php',
            method: 'post',
            data: {
               index: $(this).data('index')
            }
        });
    });
});
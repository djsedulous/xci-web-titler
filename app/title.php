<?php

$currentTitleFilePath = realpath('./data/current_title.json');
$currentTitleJson = file_get_contents($currentTitleFilePath);
$currentTitleData = json_decode($currentTitleJson, true);

function printTitle($titleData)
{
    echo <<<HTML
<span id="name">{$titleData['name']}</span>
<span id="description">{$titleData['description']}</span>
HTML;
}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
)
{
    printTitle($currentTitleData);

    die();
}

?>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/title.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        setTimeout(function() {
            document.querySelector('.titles-content').classList.add('show');

            setTimeout(function() {
                document.querySelector('.titles-content').classList.remove('show');
            }, 12800);

            setInterval(function() {
                $('.titles-content').load('title.php')
            }, 1000);
        }, 1000);
    </script>
</head>
<body>

<div class="titles-content white">
    <?php printTitle($currentTitleData); ?>
</div>

</body>
</html>
<?php

$titlesFilePath = realpath('./data/titles.json');
$currentTitleFilePath = realpath('./data/current_title.json');

$titlesJson = file_get_contents($titlesFilePath);
$titlesData = json_decode($titlesJson, true);

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
)
{
    if (isset($_POST['index']))
    {
        $index = $_POST['index'];
        if (!empty($titlesData[$index]))
        {
            $titleData = $titlesData[$index];

            $currentTitleData = [
                'index' => $index,
                'name' => $titleData['name'],
                'description' => $titleData['description'],
            ];

            file_put_contents($currentTitleFilePath, json_encode($currentTitleData));
        }
    }

    die();
}

$currentTitleJson = file_get_contents($currentTitleFilePath);
$currentTitleData = json_decode($currentTitleJson, true);

?>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="js/bootstrap-4.1.3-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>
<body>
<div class="container">
    <h4>Управление титрами</h4>

    <div class="row">
        <div class="col-lg-12 titles-list">
            <?php if (is_array($titlesData)): ?>
                <?php foreach ($titlesData as $index => $title): ?>

                    <?php
                        $additionalClass = '';
                        if (is_array($currentTitleData) && $index == $currentTitleData['index'])
                        {
                            $additionalClass = ' selected';
                        }
                    ?>

                    <div class="title-item<?= $additionalClass; ?>" data-index="<?= $index; ?>">
                        <div class="name"><?= $title['name']; ?></div>
                        <div class="description"><?= $title['description']; ?></div>
                    </div>

                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

</body>
</html>